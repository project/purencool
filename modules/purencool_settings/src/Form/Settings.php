<?php

namespace Drupal\purencool_settings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Settings page for admin variables.
 *
 * Drupal\purencool_settings\Form.
 */
class Settings extends ConfigFormBase {

  /**
   * Forms unique identifier.
   *
   * @return string
   *   Returns form unique identifier string
   */
  public function getFormId() {
    return 'purencool_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'purencool.settings',
    ];
  }

  /**
   * Build form array.
   *
   * @param array $form
   *   Adds form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Gets form state including input values.
   *
   * @return array
   *   Returns form array elements .
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('purencool.settings');

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $config->get('domain'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Validation method.
   *
   * @param array $form
   *   Array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Gets form state including input values.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * Form submit method.
   *
   * @param array $form
   *   Array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Gets form state including input values.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('purencool.settings')
      ->set('domain', $form_state->getValue('domain'))
      ->save();
  }

}
