### INTRODUCTION
Adds extra settings to profile.  

#### REQUIREMENTS

Drupal 8 or 9.

#### INSTALLATION

Install using composer then using Drush or the Drupal UI enable the module.

#### CONFIGURATION

There is no configuration.
