## INTRODUCTION

This profile has been standardised to use Drupal core and contributed modules 
with minimal custom configuration. The initial installation provides a strong 
performance focus.

It gives the developer the ability install and quickly to create a website 
using the layout manager tools and modules to minimise the need to use 
templating ( The base theme is based on Bootstrap 4). The installation 
configuration has a small footprint regarding module dependencies to ensure 
flexibility.

#### REQUIREMENTS 

This profile has been standardised to use Drupal contributed modules with
minimal custom configuration. You will need a default installation of Drupal 
and access to composer 2. 


#### INSTALLATION

Install Drupal 9 using composer and then download the profile into the Drupal 
profile directory then follow the normal install procedure through the browser 
Drupal UI and manually down required modules need for it to install.


Or to install from the terminal install this profile run the following two 
commands in the parent installation directory.

```
wget https://git.drupalcode.org/project/purencool/-/raw/9.x-0.x/scripts/installprofile  
bash installprofile  

```

#### CONFIGURATION

Install the purencool profile when it displays in the installation page.
