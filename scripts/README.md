# Script Information.

#### Quick profile install

```
 php -d memory_limit=256M web/core/scripts/drupal quick-start purencool
```

#### Drush snippets

Drush is a powerful tool that allows a developer manage the website from the 
command line. Below are some snippets that are commonly used in development.
The command below need to be run from the web root. For more information please
follow the link below.
 
###### Clear website caches

```
../vendor/drush/drush/drush cr
```

#### Coding standards

The following commands should be run before code is committed to clear all 
common know issue when developing code for Drupal. For more information please
see: https://www.drupal.org/docs/contributed-modules/code-review-module/installing-coder-sniffer

```
phpcs --standard=DrupalPractice --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml .
```

```
phpcbf --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml .
```
